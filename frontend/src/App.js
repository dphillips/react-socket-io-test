import logo from './logo.svg';
import './App.css';
import React, { useState, useEffect } from 'react';
import io from 'socket.io-client';

const socket = io('/thing');

function App() {
	//const [isConnected, setIsConnected] = useState(socket.connected);
	useEffect(() => {
		socket.on('connect', () => {
			console.log('socket connected');
			socket.emit('init', 'react here lol');
			//setIsConnected(true);
		});
		socket.on('whatever', (message) => {
			console.log('server says: ' + message);
			//setIsConnected(true);
		});
		socket.on('disconnect', () => {
			console.log('socket connected');
			//setIsConnected(false);
		});
		//return () => {
		//	console.log('teardown thingo run');
		//};
	});
	return (
		<div className="App">
			<header className="App-header">
				<img src={logo} className="App-logo" alt="logo" />
				<p>
					Edit <code>src/App.js</code> and save to reload.
				</p>
				<a
					className="App-link"
					href="https://reactjs.org"
					target="_blank"
					rel="noopener noreferrer"
				>
					Learn React
				</a>
			</header>
		</div>
	);
}

export default App;
