const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require('socket.io');
const io = new Server(server);

app.get('/', (req, res) => {
	res.sendFile(__dirname + '/frontend/build/index.html');
});

app.use(express.static(__dirname + '/frontend/build'));

io.of('/thing').on('connection', (socket) => {
	console.log('a user connected');
	socket.on('init', (message) => {
		console.log('user initted with "' + message + '"');
		socket.emit('whatever', 'yo yo yo');
	});
});

server.listen(3000, () => {
	console.log('listening');
});
